#!/bin/bash

echo "test"
COPY_DIR=$1
TMP_DIR=~/git
REPO_DIR=$TMP_DIR/illustlator_scripts
SCRIPT_DIR=$REPO_DIR/dist

mkdir -p $TMP_DIR

cd $TMP_DIR

if [ ! -e $REPO_DIR ]; then git clone https://tyagiwork@gitlab.com/tyagiwork0419/illustlator_scripts.git; fi

cd $SCRIPT_DIR
#cd $REPO_DIR/dist

# git pull origin main

# cd dist

#DEST="$REPO_DIR/dist/*.js"
FILES=$SCRIPT_DIR/*.js
mkdir -p $COPY_DIR

for filepath in $FILES; do
    echo $COPY_DIR/${filepath##*/}
    cp $filepath $COPY_DIR/${filepath##*/}
done

rm -rf $REPO_DIR