rem @echo off
set CUR_DIR=%CD%
set COPY_DIR=%1
set TMP_DIR=%HOMEDRIVE%%HOMEPATH%\git
set REPO_DIR=%TMP_DIR%\illustlator_scripts


if not exist %TMP_DIR% mkdir %TMP_DIR%

cd %TMP_DIR%

if not exist %REPO_DIR% (
    git clone https://gitlab.com/tyagiwork0419/illustlator_scripts.git
)

cd %REPO_DIR%

git pull origin main

cd dist

set DEST=%REPO_DIR%\dist\*.js

for %%f in ("%DEST%") do (
    COPY %%f %COPY_DIR%\%%~nxf

)