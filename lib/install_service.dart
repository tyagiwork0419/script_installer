import 'dart:async';
import 'dart:io';
import 'dart:convert';

class InstallService {
  InstallService() {}

  Future<void> runCommand(StreamController<String> streamController,
      String executable, List<String> arguments,
      {String? workingDirectory}) async {
    //final result = await _processManager.run(
    //  [
    //    'git',
    //    //'--no-pager',
    //    command,
    //  ],
    //  workingDirectory: workingDirecory,
    //);
    // Git Bashのパス
    String gitBashPath = "C:/Program Files/Git/bin/bash.exe";

    // 実行するシェルスクリプトのパス
    String scriptPath = "scripts/install.sh";

    // Git Bashの引数
    //List<String> args = [
    //  "--login",
    //  "-i",
    //  "-c",
    //  "bash $scriptPath",
    //];
    List<String> args = ["-i", scriptPath, "~/downloads/tmp"];

    // Git Bashを非同期で実行
    ProcessResult result = await Process.run(gitBashPath, args);

    // 結果の出力
    //print(result.stdout);
    //print(result.stderr);

    //File batFile = File('scripts\\windows\\install.bat');
    //List<String> arguments = ["\\Users\\tetuy\\downloads"];
    //final result = await Process.start(batFile.path, arguments);
    //final result = await Process.run(batFile.path, arguments);

    streamController.sink.add(result.stdout);
    streamController.sink.add(result.stderr);
    streamController.close();
    /*
    final result = await Process.start(
      executable,
      arguments,
      workingDirectory: workingDirectory,
    );
    */

    //print(
    //    'git result: ${result.exitCode.then((value) => streamController.close())}');
    //print(result.stdout);
    //print(result.stderr);

    /*
    result.exitCode.then((value) {
      print('git result: $value');
      //streamController.close();
    });
    result.stdout.listen((List<int> data) {
      String message = utf8.decode(data, allowMalformed: true);
      //print('message: $message');
      streamController.sink.add(message);
    });
    */
  }
}
