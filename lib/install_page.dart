import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'dart:async';
import 'install_service.dart';

class InstallPage extends StatefulWidget {
  late String installDirectory;
  InstallPage({super.key, required this.installDirectory});

  @override
  State<InstallPage> createState() => InstallPageState();
}

class InstallPageState extends State<InstallPage> {
  late InstallService _installService;
  late StreamController<String> _streamController;
  bool _isDone = false;
  String text = 'installing...';

  @override
  void initState() {
    super.initState();
    _installService = InstallService();
    _streamController = StreamController<String>();

    _installService.runCommand(_streamController, 'git', []);
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
        stream: _streamController.stream,
        builder: (BuildContext context, AsyncSnapshot<String> snapshot) {
          if (snapshot.hasError) {
            'Error: ${snapshot.error}';
          } else {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
                break;
              case ConnectionState.waiting:
                break;
              case ConnectionState.active:
                text = snapshot.data!;
                print('text: $text');
                break;
              case ConnectionState.done:
                text = snapshot.data!;
                print('done');
                _isDone = true;
                break;
              default:
                break;
            }
          }

          return Scaffold(
              body: Padding(
            padding: const EdgeInsets.all(10),
            child:
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
              SizedBox(
                  height: 180,
                  child: SingleChildScrollView(
                    child: Text(text),
                  )),
              Visibility(
                  visible: _isDone,
                  maintainState: true,
                  maintainSize: true,
                  maintainAnimation: true,
                  child: ElevatedButton(
                      onPressed: () => exit(0), child: const Text('finish'))),
            ]),
          ));
        });
  }
}
