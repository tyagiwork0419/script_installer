import 'dart:io';

import 'package:flutter/material.dart';
import 'package:window_size/window_size.dart';
import 'package:file_picker/file_picker.dart';

import 'install_page.dart';

const windowWidth = 500.0;
const windowHeight = 300.0;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  /*
  if (Platform.isWindows || Platform.isLinux || Platform.isMacOS) {
    getWindowInfo().then((windowInfo) {
      setWindowTitle("script_installer");
      setWindowFrame(Rect.fromCenter(
          center: windowInfo.frame.center,
          width: windowWidth,
          height: windowHeight));
      setWindowMaxSize(const Size(windowWidth, windowHeight));
      setWindowMinSize(const Size(windowWidth, windowHeight));
    });
  }
  */

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'a Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final String _defaultPath = "C:/Users/tetuy/Downloads/tmp";
  //    'C:/Program Files/Adobe/Adobe Illustrator CS6 (64 Bit)/Presets/ja_JP/スクリプト';
  String _directoryPath = '';
  late TextEditingController _controller;
  @override
  void initState() {
    super.initState();
    _directoryPath = _defaultPath;
    _controller = TextEditingController(text: _directoryPath);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //appBar: AppBar(title: Text(widget.title)),
        body: Padding(
      padding: const EdgeInsets.all(10),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const Text(
                  "スクリプトのインストール先を指定してください。",
                  textAlign: TextAlign.left,
                ),
                Text(
                  "例：$_defaultPath",
                  textAlign: TextAlign.left,
                ),
              ],
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              SizedBox(
                width: 400,
                height: 50,
                child: TextField(
                  controller: _controller,
                  decoration: const InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'インストール先:',
                  ),
                  style: const TextStyle(fontSize: 14),
                ),
              ),
              SizedBox(
                width: 60,
                height: 50,
                child: ElevatedButton(
                  onPressed: () async {
                    //final directory = await getExternalStorageDirectory();
                    final result = await FilePicker.platform
                        .getDirectoryPath(initialDirectory: _directoryPath);
                    if (result != null) {
                      setState(() {
                        _directoryPath = result.toString();
                        _controller.text = _directoryPath;
                      });
                    }
                  },
                  child: const Text('参照'),
                ),
              ),
            ]),
            ElevatedButton(
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              InstallPage(installDirectory: _directoryPath)));
                },
                child: const Text("インストール"))
          ]),
    ));
  }
}
